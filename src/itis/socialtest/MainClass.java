package itis.socialtest;


import itis.socialtest.entities.Author;
import itis.socialtest.entities.Post;

import java.io.*;
import java.util.ArrayList;
import java.util.List;


/*
 * В папке resources находятся два .csv файла.
 * Один содержит данные о постах в соцсети в следующем формате: Id автора, число лайков, дата, текст
 * Второй содержит данные о пользователях  - id, никнейм и дату рождения
 *
 * Напишите код, который превратит содержимое файлов в обьекты в package "entities"
 * и осуществите над ними следующие опреации:
 *
 * 1. Выведите в консоль все посты в читабельном виде, с информацией об авторе.
 * 2. Выведите все посты за сегодняшнюю дату
 * 3. Выведите все посты автора с ником "varlamov"
 * 4. Проверьте, содержит ли текст хотя бы одного поста слово "Россия"
 * 5. Выведите никнейм самого популярного автора (определять по сумме лайков на всех постах)
 *
 * Для выполнения заданий 2-5 используйте методы класса AnalyticsServiceImpl (которые вам нужно реализовать).
 *
 * Требования к реализации: все методы в AnalyticsService должны быть реализованы с использованием StreamApi.
 * Использование обычных циклов и дополнительных переменных приведет к снижению баллов, но допустимо.
 * Парсинг файлов и реализация методов оцениваются ОТДЕЛЬНО
 *
 *
 * */

public class MainClass {

    private ArrayList<Post> allPosts = new ArrayList<>();
    private ArrayList<Author> allAuthors = new ArrayList<>();

    private AnalyticsService analyticsService = new AnalyticsServiceImpl();

    public MainClass() {
    }

    public static void main(String[] args) {
        new MainClass().run("", "");
    }

    private void run(String postsSourcePath, String authorsSourcePath) {
        try {
            BufferedReader postsBr = new BufferedReader(new FileReader(postsSourcePath));
            BufferedReader authorsBr = new BufferedReader(new FileReader(authorsSourcePath));
            int authorId = 0;
            long likesCount;
            String date;
            StringBuilder content = new StringBuilder();
            long userId = 0;
            String username;
            String birthDate;
            ArrayList<String[]> dataLines = new ArrayList<>();
            String line;
            String authorNickname;
            String authorBirthDate;



            //Парсинг автора
            while ((line = authorsBr.readLine()) != null) {
                dataLines.add(line.split("\n"));
            }
            authorsBr.close();

            for (String[] dataLine : dataLines) {
                String[] currLine = dataLine[0].split(", ");
                userId = Long.parseLong(currLine[0]);
                username = currLine[1];
                birthDate = currLine[2];
                Author author = new Author(userId, username, birthDate);
                allAuthors.add(author);
            }

            //Парсинг поста
            dataLines.clear();
            while ((line = postsBr.readLine()) != null) {
                dataLines.add(line.split("\n"));
            }
            postsBr.close();

            for (String[] dataLine : dataLines) {
                String[] currLine = dataLine[0].split(", ");
                authorId = Integer.parseInt(currLine[0]);
                likesCount = Long.parseLong(currLine[1]);
                date = currLine[2];
                for (int j = 3; j < currLine.length; j++) {
                    content.append(currLine[j]);
                }


                for (Author allAuthor : allAuthors) {
                    if (allAuthor.getId() == authorId) {
                        Post post = new Post(date, content.toString(), likesCount, allAuthor);
                        allPosts.add(post);
                    }
                }
            }

            System.out.println("1 task");
            printAllPosts();
            System.out.println("2 task");
            printPostsByDate();
            System.out.println("3 task");
            printPostsByAuthor();
            System.out.println("4 task");
            printCheckingThatContentContainsSearchString();
            System.out.println("5 task");
            printMostPopularAuthorNickname();




        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void printAllPosts(){
        for (Post post :
                allPosts) {
            System.out.println("\nПост от " + post.getAuthor().getNickname() + ". Текст: " + post.getContent() +
                    ". Количество лайков: " + post.getLikesCount() + "Дата создания: " + post.getDate()
                    + "\nИнформация об авторе: " + "\nID автора: " + post.getAuthor().getId() +
                    "\nНикнейм автора: " + post.getAuthor().getNickname()
                    + "\nДата рождения автора: " + post.getAuthor().getBirthdayDate());
        }
    }
    private void printPostsByDate(){
        System.out.println(analyticsService.findPostsByDate(allPosts, "16.04.2021T10:00"));
    }

    private void printPostsByAuthor(){
        System.out.println(analyticsService.findAllPostsByAuthorNickname(allPosts,"varlamov"));
    }

    private void printCheckingThatContentContainsSearchString(){
        if(analyticsService.checkPostsThatContainsSearchString(allPosts, "Россия")){
            System.out.println("Да, содержит");
        }
        System.out.println("Не содержит");
    }

    private void printMostPopularAuthorNickname(){
        System.out.println(analyticsService.findMostPopularAuthorNickname(allPosts));
    }
}
